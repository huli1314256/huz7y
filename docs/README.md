---
home: true
title: 主页
heroImage: https://vuejs.press/images/hero.png
actions:
  - text: 开始
    link: /getting-started.html
    type: primary

  - text: Gitee
    link: https://gitee.com/fox-glaze
    type: secondary

footer: HL | 版权所有 © 2024 huli
---